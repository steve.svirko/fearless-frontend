function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
        <div class="col h-auto">
            <div class="card shadow p-0 mb-3 bg-body rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">${starts}-${ends}</small>
                </div>
            </div>
        </div>
        `;
    }




window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const starts = new Date(details.conference.starts).toLocaleDateString()
            const ends = new Date(details.conference.ends).toLocaleDateString()
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, location, starts, ends);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }
  
      }
    } catch (e) {
      alert('STOP HAMMERTIME')
    }
  
  });